
window.onload = () => {
    /* Створіть 2 інпути та одну кнопку. Зробіть так, щоб інпути обмінювалися вмістом.*/

    const [...inputs] = document.querySelectorAll("#name");

    const nameButton = document.querySelector("#nameButton");

    function clik() {

        const a = inputs[0].value
        const b = inputs[1].value
        inputs[0].value=b
        inputs[1].value=a

    }
    nameButton.onclick = clik
        
    



    /*
     Створіть 5 див на сторінці потім використовуючи getElementsByTagName і forEath поміняйте дивам колір фону.
     */


    const div = document.querySelector(".div");

    for (let i = 0; i < 5; i++) {
        const clas = document.createElement("div");
        div.append(clas)
        clas.setAttribute("class", "createdDiv");

    }
    const [...createdDiv] = div.getElementsByTagName("div");


    createdDiv.forEach((el, i) => {
        el.innerText = `Це створений <div> під номером ${i + 1}`;
        el.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 100%, 50%)`
    });



    /* 
    Створіть багаторядкове поле для введення тексту та кнопки. Після натискання кнопки користувачем програма повинна
   згенерувати тег div з текстом, який був у багаторядковому полі. багаторядкове поле слід очистити після
   переміщення інформації
   */


    const textArea = document.querySelector("#textareaInput");

    const textButton = document.querySelector("#textButton");
    const divText = document.querySelector(".textarea");
    const form = document.querySelector(".form-text");



    function clik2() {
        const generatedDiv = document.createElement("div");
        divText.append(generatedDiv)
        generatedDiv.setAttribute("class", "generated");
        generatedDiv.innerText = textArea.value;
        form.reset()


    }
    textButton.onclick = clik2;


    /* Створіть картинку та кнопку з назвою "Змінити картинку"
    зробіть так щоб при завантаженні сторінки була картинка
    https://itproger.com/img/courses/1476977240.jpg
    При натисканні на кнопку вперше картинка замінилася на
    https://itproger.com/img/courses/1476977488.jpg
    при другому натисканні щоб картинка замінилася на
    https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png
    */

    const img = document.querySelector(".image >img");
    const btnImg = document.querySelector(".image > button");
    var imgs = new Array("https://itproger.com/img/courses/1476977240.jpg", "https://itproger.com/img/courses/1476977488.jpg", "http://surl.li/evmay")

    let i = 0;
    function clik3() {
        //дана умова працює для зациклення картинок
        i++;
        if (i == imgs.length) {
            i = 0;
        }

        //ця умова при закінчення картинок - зміна зупиняється 
        /*if (i!=((imgs.length)-1)){
            i++
        }*/
        img.setAttribute("src", imgs[i])

    }
    btnImg.onclick = clik3;

    /*
    Створіть на сторінці 10 параграфів і зробіть так, щоб при натисканні на параграф він зникав
    */
    const paragraf = document.querySelector(".paragraf");
    const [...paragraphs] = paragraf.querySelectorAll("p");

    function clean() {

        this.style.display = 'none'

    }
    paragraphs.forEach((el) => { el.onclick = clean })


/*
Намалювати на сторінці коло за допомогою параметрів, які введе користувач.
При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло".Дана кнопка повинна бути єдиним контентом у тілі HTML документа, решта контенту повинен бути створений і доданий на сторінку за допомогою Javascript
При натисканні кнопки "Намалювати коло" показувати одне поле введення - діаметр кола.При натисканні на кнопку "Намалювати" створити на сторінці 100 кіл(10 * 10) випадкового кольору.При натисканні на конкретне коло - це коло повинен зникати, при цьому порожнє місце заповнюватися, тобто всі інші кола зрушуються вліво
*/


const circuitWrapper = document.querySelector(".circuit");
const btnCircuit = document.querySelector("#btnCircuit");

function circuit() {
    const circuitItem = document.createElement("div");
    circuitWrapper.append(circuitItem)
    circuitItem.setAttribute("class", "circuit-item");
    let a = parseInt(prompt("Введіть діаметр кіл, які необхідно побудувати (від 1 до 150)", 10))
    if(a>150){
        a=150
    }
   
    for (let i = 0; i < 100; i++) {
        const circle = document.createElement("div");
        circuitItem.prepend(circle)
    }
    const [...circles]=circuitItem.querySelectorAll("div");
    circles.forEach((el)=>el.style.height=`${a}px`)
    circles.forEach((el)=>el.style.width=`${a}px`)
    circles.forEach((el)=>el.style.boxSizing="content-box")
    function getRandom(min, max){
        return Math.ceil(Math.random() * (max - min) + min)
      }
    circles.forEach((el)=>el.style.backgroundColor = `rgb(${getRandom(0, 255)}, ${getRandom(0, 255)}, ${getRandom(0, 255)})`);
    circles.forEach((el) => { el.onclick = clean })

}
btnCircuit.onclick=circuit

}